#include <tuple>
#include <cstddef>
#include <type_traits>

constexpr auto Dimensions = std::make_index_sequence<6>{};

// Define a list of types for the vector elements. Include `long double` only if it's different from `double`.
using FloatTypes = std::tuple<
        float,
        double,
        std::conditional_t<!std::is_same_v<double, long double>, long double, void>
>;

using IntegerTypes = std::tuple<char, short, int, long, long long>;

using ElementTypes = decltype(std::tuple_cat(FloatTypes{}, IntegerTypes{}));

template<typename T, std::size_t... Ns>
auto make_vectors_for_type(std::index_sequence<Ns...>) {
    return std::tuple<ultimate_container::vector::Vector<T, Ns + 1>...>{};
}

template<typename... Types, std::size_t... Ns>
auto make_all_vectors(std::tuple<Types...>, std::index_sequence<Ns...> sequence) {
    return std::tuple_cat(make_vectors_for_type<Types>(sequence)...);
}

using AllVectorTypes = decltype(make_all_vectors(ElementTypes{}, std::make_index_sequence<6>{}));
