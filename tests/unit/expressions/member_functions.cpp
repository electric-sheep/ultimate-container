#include <catch2/catch_all.hpp>
#include "vector/vector.h"

using namespace ultimate_container::vector;
using namespace ultimate_container::vector::operations;

TEST_CASE("Expressions: Member Function Operations", "[Expressions][Member]") {
    Vector<int, 3> a = {1, 2, 3};
    Vector<int, 3> b = {4, 5, 6};

    SECTION("Component-wise Product") {
        auto c = a.cwise_product(b);
        REQUIRE(c[0] == 4);
        REQUIRE(c[1] == 10);
        REQUIRE(c[2] == 18);
    }

    SECTION("Component-wise Division") {
        auto c = a.cwise_divide(b);
        REQUIRE(c[0] == 0);
        REQUIRE(c[1] == 0);
        REQUIRE(c[2] == 0);
    }

    SECTION("Component-wise Min") {
        auto c = a.cwise_min(b);
        REQUIRE(c[0] == 1);
        REQUIRE(c[1] == 2);
        REQUIRE(c[2] == 3);
    }

    SECTION("Component-wise Max") {
        auto c = a.cwise_max(b);
        REQUIRE(c[0] == 4);
        REQUIRE(c[1] == 5);
        REQUIRE(c[2] == 6);
    }
}
