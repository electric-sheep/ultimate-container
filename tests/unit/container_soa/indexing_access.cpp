#include <catch2/catch_all.hpp>
#include <complex>
#include "container_soa/container_soa.h"
#include "vector/vector.h"

using namespace ultimate_container::container_soa;
using namespace ultimate_container::vector;
using namespace ultimate_container::vector::operations;

//TODO: Add all types including std::complex
TEMPLATE_TEST_CASE("ContainerSoA Indexing", "[ContainerSoa][Indexing]", int, float, double) {
    using TestVector = Vector<TestType, 3>;
    using IndexVector = Vector<std::size_t, 3>;

    constexpr std::size_t CONTAINER_DIM = 3;
    constexpr std::array<std::size_t, CONTAINER_DIM> dims = {10, 5, 2};

    ContainerSoa<TestVector , CONTAINER_DIM> container(dims[0], dims[1], dims[2]);
    REQUIRE(container.size() == dims);
    for (std::size_t i = 0; i < dims[0]; ++i) {
        for (std::size_t j = 0; j < dims[1]; ++j) {
            for (std::size_t k = 0; k < dims[2]; ++k) {
                container[i, j, k] = TestVector(static_cast<TestType>(dims[0]),
                                               static_cast<TestType>(dims[1]),
                                               static_cast<TestType>(dims[2]));
            }
        }
    }

    for (std::size_t i = 0; i < dims[0]; ++i) {
        for (std::size_t j = 0; j < dims[1]; ++j) {
            for (std::size_t k = 0; k < dims[2]; ++k) {
                auto test_vector = TestVector(static_cast<TestType>(dims[0]),
                                              static_cast<TestType>(dims[1]),
                                              static_cast<TestType>(dims[2]));
                auto index_vector = IndexVector{i, j, k};
                REQUIRE(container[i, j, k] == test_vector);
                REQUIRE(container[index_vector] == test_vector);
                REQUIRE(container[IndexVector{i, j, k}] == test_vector);
                REQUIRE(container[i, j, k] == container[index_vector]);
                REQUIRE(container[i, j, k] == container[IndexVector{i, j, k}]);
                REQUIRE(container[index_vector] == container[IndexVector{i, j, k}]);
                container[index_vector] += TestVector{1, 1, 1};
                REQUIRE(container[index_vector] == test_vector + TestVector{1, 1, 1});
            }
        }
    }


}