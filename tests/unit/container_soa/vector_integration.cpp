#include <catch2/catch_all.hpp>
#include <complex>
#include "container_soa/container_soa.h"
#include "vector/vector.h"

using namespace ultimate_container::container_soa;
using namespace ultimate_container::vector;

//TODO: Add all types including std::complex
TEMPLATE_TEST_CASE("ContainerSoA for Vectors", "[ContainerSoa][Vector]", int, float, double) {
    using TestVector = Vector<TestType, 3>;

    constexpr std::size_t CONTAINER_DIM = 3;
    constexpr std::array<std::size_t, CONTAINER_DIM> dims = {10, 5, 2};

//    SECTION("Default Initialization") {
//        ContainerSoa<Scalar, DIM> container;
//        REQUIRE(container.size() == std::array<std::size_t, DIM>{0, 0, 0});
//    }

    SECTION("Default Initialization") {
        ContainerSoa<TestVector , CONTAINER_DIM> container(dims[0], dims[1], dims[2]);
        REQUIRE(container.size() == dims);
        for (std::size_t i = 0; i < dims[0]; ++i) {
            for (std::size_t j = 0; j < dims[1]; ++j) {
                for (std::size_t k = 0; k < dims[2]; ++k) {
                    for (std::size_t l = 0; l < 3; ++l) {
                        REQUIRE(container[i, j, k][l] == TestType{});
                    }
                }
            }
        }
    }

    SECTION("Copy Initialization") {
        ContainerSoa<TestVector , CONTAINER_DIM> original(dims[0], dims[1], dims[2]);
        for (std::size_t i = 0; i < dims[0]; ++i) {
            for (std::size_t j = 0; j < dims[1]; ++j) {
                for (std::size_t k = 0; k < dims[2]; ++k) {
                    original[i, j, k] = TestVector(static_cast<TestType>(dims[0]),
                                                   static_cast<TestType>(dims[1]),
                                                   static_cast<TestType>(dims[2]));
                }
            }
        }
        ContainerSoa<TestVector , CONTAINER_DIM> copy = original;
        REQUIRE(copy.size() == original.size());
        for (std::size_t i = 0; i < dims[0]; ++i) {
            for (std::size_t j = 0; j < dims[1]; ++j) {
                for (std::size_t k = 0; k < dims[2]; ++k) {
                    REQUIRE(copy[i, j, k] == TestVector(static_cast<TestType>(dims[0]),
                                                        static_cast<TestType>(dims[1]),
                                                        static_cast<TestType>(dims[2])));
                }
            }
        }
    }

    SECTION("Move Initialization") {
        ContainerSoa<TestVector , CONTAINER_DIM> original(dims[0], dims[1], dims[2]);
        for (std::size_t i = 0; i < dims[0]; ++i) {
            for (std::size_t j = 0; j < dims[1]; ++j) {
                for (std::size_t k = 0; k < dims[2]; ++k) {
                    original[i, j, k] = TestVector(static_cast<TestType>(dims[0]),
                                                   static_cast<TestType>(dims[1]),
                                                   static_cast<TestType>(dims[2]));
                }
            }
        }
        ContainerSoa<TestVector, CONTAINER_DIM> moved = std::move(original);
        REQUIRE(moved.size() == dims);
        for (std::size_t i = 0; i < dims[0]; ++i) {
            for (std::size_t j = 0; j < dims[1]; ++j) {
                for (std::size_t k = 0; k < dims[2]; ++k) {
                    REQUIRE(moved[i, j, k] == TestVector(static_cast<TestType>(dims[0]),
                                                         static_cast<TestType>(dims[1]),
                                                         static_cast<TestType>(dims[2])));
                }
            }
        }
    }

    SECTION("Modification"){
        ContainerSoa<TestVector , CONTAINER_DIM> container(dims[0], dims[1], dims[2]);
        for (std::size_t i = 0; i < dims[0]; ++i) {
            for (std::size_t j = 0; j < dims[1]; ++j) {
                for (std::size_t k = 0; k < dims[2]; ++k) {
                    container[i, j, k] = TestVector(static_cast<TestType>(dims[0]),
                                                    static_cast<TestType>(dims[1]),
                                                    static_cast<TestType>(dims[2]));
                }
            }
        }
        for (std::size_t i = 0; i < dims[0]; ++i) {
            for (std::size_t j = 0; j < dims[1]; ++j) {
                for (std::size_t k = 0; k < dims[2]; ++k) {
                    container[i, j, k] += TestVector{1, 1, 1};
                }
            }
        }
        for (std::size_t i = 0; i < dims[0]; ++i) {
            for (std::size_t j = 0; j < dims[1]; ++j) {
                for (std::size_t k = 0; k < dims[2]; ++k) {
                    REQUIRE(container[i, j, k] == TestVector(static_cast<TestType>(dims[0] + 1),
                                                             static_cast<TestType>(dims[1] + 1),
                                                             static_cast<TestType>(dims[2] + 1)));
                }
            }
        }
    }

}