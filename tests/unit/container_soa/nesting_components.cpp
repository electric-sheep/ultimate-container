#include "test_particle.h"

#include "container_soa/container_soa.h"
#include "utils/string.h"

#include <catch2/catch_all.hpp>
#include <string>

using namespace ultimate_container::container_soa;
using namespace ultimate_container::vector;
using namespace ultimate_container;



void check_arithmetic_component(const auto& component,
                                std::string& output_string,
                                const std::vector<std::string>& path_tokens) {
    component.apply_to_components_with_nesting(
            [&](const auto &flat_component, std::string_view flat_variable_path) {
                output_string += flat_variable_path;
                output_string += ": ";
                for(std::size_t i = 0; i < flat_component.size(); ++i){
                    output_string += std::to_string(flat_component[i]);
                    if(i < flat_component.size() - 1){
                        output_string += ", ";
                    }
                }
                output_string += "\n";
            }, utils::join(path_tokens, "/")
    );
}

template<typename T, std::size_t DIM, typename Index = std::size_t>
void check_component(const container_soa::ContainerSoa<T, DIM>& container,
                            std::string& output_string,
                            std::string_view parent_group_name = "") {

    if constexpr (Arithmetic<typename std::decay_t<decltype(container)>::value_type>){
        //TODO: Same comment as in hdf5.h, see issue #10
        check_arithmetic_component(container, output_string, utils::split(std::string(parent_group_name) + "/0", "/"));
    } else {
        container.apply_to_components_with_nesting([&](const auto &component, std::string_view variable_path) {
            auto tokens = utils::split(std::string(variable_path), "/");
            using ComponentType = std::decay_t<decltype(component)>;
            if constexpr (Arithmetic<typename ComponentType::value_type>) {
                check_arithmetic_component(component, output_string, tokens);
            } else {
                check_component(component, output_string, variable_path);
            }
        }, parent_group_name);
    }
}

std::string expected_output =
R"(Particles/0/0: 1.000000, 5.000000
Particles/0/1: 2.000000, 6.000000
Particles/0/2: 3.000000, 7.000000
Particles/1: 4.000000, 8.000000
Particles/2: 0, 1
)";

TEST_CASE("ContainerSoA Nested Components Traversal", "[ContainerSoa][Nesting]"){
    using TestParticle = ParticleT<Vector<double, 3>, double>;
    using TestContainer = ContainerSoa<TestParticle, 1>;

    TestContainer container(2);
    REQUIRE(container.size() == std::array<std::size_t, 1>{2});
    container[0] = TestParticle(Vector<double, 3>{1, 2, 3}, 4, 0);
    container[1] = TestParticle(Vector<double, 3>{5, 6, 7}, 8, 1);

    std::string output_string;
    check_component(container, output_string, "Particles");

    REQUIRE(output_string == expected_output);
}
