#include <tuple>
#include <type_traits>
#include <catch2/catch_all.hpp>
#include "vector/vector.h"
#include "vector_test_matrix.h"

using namespace ultimate_container::vector;

template<typename TVector, std::size_t... Is>
void test_vector_constructor(std::index_sequence<Is...>) {
    using ValueType = typename TVector::value_type;
    constexpr std::size_t dim = sizeof...(Is);
    assert (dim == TVector::dimension);
    TVector v(static_cast<ValueType>(Is + 1)...);

    for (std::size_t i = 0; i < dim; ++i) REQUIRE(v[i] == ValueType(i + 1));
}

template<typename TVector, std::size_t... Is>
void test_vector_initializer_list(std::index_sequence<Is...>) {
    using ValueType = typename TVector::value_type;
    constexpr std::size_t dim = sizeof...(Is);
    assert (dim == TVector::dimension);
    TVector v = {static_cast<ValueType>(Is + 1)...};

    for (std::size_t i = 0; i < dim; ++i) REQUIRE(v[i] == ValueType(i + 1));
}

TEST_CASE("Vector Basic Constructor Test", "[vector][constructor]") {
    Vector<int, 3> a;
    a[0] = 1; a[1] = 2; a[2] = 3;
    Vector<int, 3> b = {1, 2, 3};
    Vector<int, 3> c(1, 2, 3);
    for(int i = 0; i < 3; ++i){
        REQUIRE(a[i] == b[i]);
        REQUIRE(a[i] == c[i]);
        REQUIRE(a[i] == i + 1);
    }
}

TEMPLATE_LIST_TEST_CASE("Vector Default Constructor Test", "[vector][default_constructor]", AllVectorTypes) {
    using TestVector = TestType;
    using ValueType = typename TestVector::value_type;
    constexpr std::size_t dim = TestVector::dimension;

    SECTION("Assignment and copies") {
        TestVector v_orig;
        for (std::size_t i = 0; i < dim; ++i) v_orig[i] = i;
        for (std::size_t i = 0; i < dim; ++i) REQUIRE(v_orig[i] == ValueType(i));

        TestVector v_copy(v_orig);
        for (std::size_t i = 0; i < dim; ++i) REQUIRE(v_copy[i] == ValueType(i));

        TestVector v_move(std::move(v_copy));
        for (std::size_t i = 0; i < dim; ++i) REQUIRE(v_move[i] == ValueType(i));

        TestVector v_copy_assign = v_orig;
        for (std::size_t i = 0; i < dim; ++i) REQUIRE(v_copy_assign[i] == ValueType(i));

        TestVector v_move_assign = std::move(v_copy_assign);
        for (std::size_t i = 0; i < dim; ++i) REQUIRE(v_move_assign[i] == ValueType(i));
    }

    SECTION("Value initialization") {
        test_vector_constructor<TestVector>(std::make_index_sequence<TestVector::dimension>{});
        test_vector_initializer_list<TestType>(std::make_index_sequence<TestType::dimension>{});
    }

    SECTION("Assignment from std::array") {
        std::array<ValueType, dim> arr;
        for (std::size_t i = 0; i < dim; ++i) arr[i] = static_cast<ValueType>(i + 1);

        TestVector v_from_array(arr);
        for (std::size_t i = 0; i < dim; ++i) REQUIRE(v_from_array[i] == arr[i]);
    }

    SECTION("Type conversion and casting") {
        using OtherVector = Vector<int, dim>;
        OtherVector v_orig;
        for (std::size_t i = 0; i < dim; ++i) v_orig[i] = static_cast<int>(i + 1);

        TestVector v_converted(v_orig);
        for (std::size_t i = 0; i < dim; ++i) REQUIRE(v_converted[i] == static_cast<ValueType>(v_orig[i]));
    }

    SECTION("Self-assignment") {
        TestVector v_orig;
        for (std::size_t i = 0; i < dim; ++i) v_orig[i] = static_cast<ValueType>(i + 1);

        v_orig = v_orig;
        for (std::size_t i = 0; i < dim; ++i) REQUIRE(v_orig[i] == static_cast<ValueType>(i + 1));
    }
}
