#include "ultimate_container.h"
#include "ultimate_container/serialization/hdf5.h"

#include <iostream>
#include <random>
#include <chrono>

using namespace ultimate_container::container_soa;
using namespace ultimate_container::vector;
using namespace ultimate_container::vector::operations;

using ticker = std::chrono::high_resolution_clock;
using std::chrono::duration;


constexpr std::size_t PROBLEM_SIZE = 1000000;
constexpr auto DIM_POS = 2;
constexpr auto DIM_FLD = 3;
using Scalar = double;

using VPOS = Vector<Scalar,DIM_POS>;
using VFLD = Vector<Scalar,DIM_FLD>;

template<typename T_POS = VPOS,
         typename T_VEL = VFLD,
         typename T_MASS = Scalar>
struct ParticleT
{
    ParticleT(T_POS::const_reference_vector in_r,
              T_VEL::const_reference_vector in_u,
              const T_MASS& in_igamma)
            : pos(in_r),
              vel(in_u),
              mass(in_igamma)
    {}

    ParticleT(T_POS::reference_vector in_r,
              T_VEL::reference_vector in_u,
              T_MASS& in_igamma)
            : pos(in_r),
              vel(in_u),
              mass(in_igamma)
    {}

    ParticleT()
        : pos(T_POS::Zeros()),
          vel(T_VEL::Zeros()),
          mass(0)
    {}

    template<typename T_POS2, typename T_VEL2, typename T_MASS2>
    ParticleT& operator=(const ParticleT<T_POS2, T_VEL2, T_MASS2>& p){
        pos = p.pos;
        vel = p.vel;
        mass = p.mass;
        return *this;
    }

    T_POS pos;
    T_VEL vel;
    T_MASS mass;

    using reference = ParticleT<typename T_POS::reference_vector, typename T_VEL::reference_vector, T_MASS&>;
    using const_reference = ParticleT<typename T_POS::const_reference_vector, typename T_VEL::const_reference_vector, const T_MASS&>;
};

using Particle = ParticleT<>;

namespace ultimate_container::container_soa {
template<std::size_t DIM>
struct ContainerSoaTraits<Particle, DIM> {
  using value_type = Particle;
  using reference  = Particle::reference;
  using const_reference = Particle::const_reference;
  using soa_tuple = std::tuple<
          ContainerSoa<VPOS, DIM>,
          ContainerSoa<VFLD, DIM>,
          ContainerSoa<Scalar, DIM>
  >;

};
}

using ParticleStorage = ContainerSoa<Particle, 1>;

int main()
{
    auto particles = ParticleStorage();

    {
        const auto start = ticker::now();
        particles.resize(PROBLEM_SIZE);

        std::default_random_engine engine(42);
        std::normal_distribution<Scalar> dist(Scalar(0), Scalar(1));
        for (std::size_t i = 0; i < PROBLEM_SIZE; ++i)
        {
            Particle::reference p = particles[i];
            p.pos = VPOS(dist(engine), dist(engine));
            p.vel = 0.1f * VFLD(dist(engine), dist(engine), dist(engine));
            p.mass = 0.01f * dist(engine);
        }

        std::cout << "Initialization took " << duration<double>(ticker::now() - start).count() << " s." << std::endl;
    }

    {
        const auto start = ticker::now();
        auto file = ultimate_container::serialization::create_output_file("data");
        ultimate_container::serialization::serialize_to_hdf5(particles, file, "particles");
        std::cout << "Output took " << duration<double>(ticker::now() - start).count() << " s." << std::endl;
    }

    return 0;
}
