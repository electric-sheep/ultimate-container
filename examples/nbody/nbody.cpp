#define USING_ULTIMATE_OPERATIONS 1
#include "ultimate_container.h"

#include <iostream>
#include <random>
#include <chrono>

#define PARTICLE_SOA

using namespace ultimate_container::container_soa;
using namespace ultimate_container::vector;

#ifdef NDEBUG
constexpr std::size_t PROBLEM_SIZE = 8 * 1024; ///< total number of particles
constexpr auto STEPS = 20; ///< number of steps to calculate
#else
constexpr std::size_t PROBLEM_SIZE = 16; ///< total number of particles
constexpr auto STEPS = 4; ///< number of steps to calculate
#endif

constexpr auto DIM_POS = 3;
constexpr auto DIM_FLD = 3;
using Scalar = float;

using VPOS = Vector<Scalar,DIM_POS>;
using VFLD = Vector<Scalar,DIM_FLD>;

template<typename T_POS = VPOS,
         typename T_VEL = VFLD,
         typename T_MASS = Scalar>
struct ParticleT
{
    ParticleT(T_POS::const_reference_vector in_r,
              T_VEL::const_reference_vector in_u,
              const T_MASS& in_igamma)
            : pos(in_r),
              vel(in_u),
              mass(in_igamma)
    {}

    ParticleT(T_POS::reference_vector in_r,
              T_VEL::reference_vector in_u,
              T_MASS& in_igamma)
            : pos(in_r),
              vel(in_u),
              mass(in_igamma)
    {}

    ParticleT()
        : pos(T_POS::Zeros()),
          vel(T_VEL::Zeros()),
          mass(0)
    {}

    template<typename T_POS2, typename T_VEL2, typename T_MASS2>
    ParticleT& operator=(const ParticleT<T_POS2, T_VEL2, T_MASS2>& p){
        pos = p.pos;
        vel = p.vel;
        mass = p.mass;
        return *this;
    }

    T_POS pos;
    T_VEL vel;
    T_MASS mass;

#ifdef PARTICLE_SOA
    using reference = ParticleT<typename T_POS::reference_vector, typename T_VEL::reference_vector, T_MASS&>;
    using const_reference = ParticleT<typename T_POS::const_reference_vector, typename T_VEL::const_reference_vector, const T_MASS&>;
#else
    using reference = ParticleT&;
    using const_reference = const ParticleT&;
#endif

};

using Particle = ParticleT<>;


#ifdef PARTICLE_SOA

namespace ultimate_container::container_soa {
template<std::size_t DIM>
struct ContainerSoaTraits<Particle, DIM> {
  using value_type = Particle;
  using reference  = Particle::reference;
  using const_reference = Particle::const_reference;
  using soa_tuple = std::tuple<
          ContainerSoa<VPOS, DIM>,
          ContainerSoa<VFLD, DIM>,
          ContainerSoa<Scalar, DIM>
  >;

};
}

using ParticleStorage = ContainerSoa<Particle, 1>;

#else

using ParticleStorage = std::vector<Particle>;

#endif


constexpr Scalar EPS2 = 0.01f;

void pPInteraction(Particle::reference p1, Particle::reference p2, Scalar ts)
{
    VFLD dist = p1.pos - p2.pos;
    dist = cwise_product(dist, dist);
    const Scalar distSqr = EPS2 + dist[0] + dist[1] + dist[2];  // This was a deliberate design decision to have N-D vectors indexed by number, instead of implementing the x, y, z (it was one or the other, not both, and [i] seemed more flexible)
    const Scalar distSixth = distSqr * distSqr * distSqr;
    const Scalar invDistCube = 1.0f / std::sqrt(distSixth);
    const Scalar s = p2.mass * invDistCube;
    p1.vel += s*ts*dist;
}

template <typename View>
void update(View& particles, Scalar ts){
    for (std::size_t i = 0; i < PROBLEM_SIZE; i++){
        #pragma GCC ivdep
        for (std::size_t j = 0; j < PROBLEM_SIZE; j++){
            pPInteraction(particles[j], particles[i], ts);
        }
    }
}

template <typename View>
void move(View& particles, Scalar ts){
    #pragma GCC ivdep
    for (std::size_t i = 0; i < PROBLEM_SIZE; i++){
        particles[i].pos += particles[i].vel * ts;
    }
}

int main()
{
    constexpr Scalar ts = 0.0001f;

    std::cout << PROBLEM_SIZE / 1000 << " thousand particles\n";

    auto particles = ParticleStorage();

    {
        const auto start = std::chrono::high_resolution_clock::now();
        particles.resize(PROBLEM_SIZE);
        const auto stop = std::chrono::high_resolution_clock::now();
        std::cout << "alloc took " << std::chrono::duration<double>(stop - start).count() << "s" << std::endl;
    }

    {
        const auto start = std::chrono::high_resolution_clock::now();

        std::default_random_engine engine(42);
        std::normal_distribution<Scalar> dist(Scalar(0), Scalar(1));
        for (std::size_t i = 0; i < PROBLEM_SIZE; ++i)
        {
            Particle::reference p = particles[i];
            p.pos = VPOS(dist(engine), dist(engine), dist(engine));
            p.vel = 0.1f * VFLD(dist(engine), dist(engine), dist(engine));
            p.mass = 0.01f * dist(engine);
        }

        const auto stop = std::chrono::high_resolution_clock::now();
        std::cout << "init took " << std::chrono::duration<double>(stop - start).count() << "s" << std::endl;
    }

    std::cout << "particles[0].pos = " << particles[0].pos << std::endl;
    std::cout << "running " << STEPS << " steps\n";
    double update_time = 0;
    double move_time = 0;
    for (std::size_t s = 0; s < STEPS; ++s)
    {
        {
            const auto start = std::chrono::high_resolution_clock::now();
            update(particles, ts);
            const auto stop = std::chrono::high_resolution_clock::now();
            update_time += std::chrono::duration<double>(stop - start).count();
            std::cout << s << std::flush;
        }
        {
            const auto start = std::chrono::high_resolution_clock::now();
            move(particles, ts);
            const auto stop = std::chrono::high_resolution_clock::now();
            move_time += std::chrono::duration<double>(stop - start).count();
            std::cout << ", " << std::flush;
        }
    }
    std::cout << "done." << std::endl;
    std::cout << "update took " << update_time << "s\n";
    std::cout << "move took: " << move_time << "s\n";

    std::cout << "particles[0].pos = " << particles[0].pos << std::endl;

    return 0;
}
