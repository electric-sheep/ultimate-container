#include "ultimate_container.h"

#include "cmd_line_parser.h"
#include "xdmf_writer.h"

#include <iostream>
#include <cmath>
#include <numbers>
#include <print>

#define PARTICLE_SOA

using namespace ultimate_container::vector::operations;
using ultimate_container::container_soa::rot;

#ifdef HAS_HDF5
using namespace ultimate_container::serialization;
#endif

using RealVector = ultimate_container::vector::Vector<float, 3>;
using IndexVector = ultimate_container::vector::Vector<int,  3>;

using VectorField = ultimate_container::container_soa::ContainerSoa<RealVector, 3>;

template<typename T> constexpr inline T sqr(T x){ return x * x; }
const float pi = std::numbers::pi_v<float>;
const float c = 1;
const float dt_factor = (float)0.99;

class Grid {
public:
    VectorField E;
    VectorField J;
    VectorField B;

    Grid(const IndexVector& size, const RealVector& delta_r)
        : E(size),
          J(size),
          B(size),
          _size(size),
          _dr(delta_r){
    }

    [[nodiscard]] auto size()   const { return _size; }
    [[nodiscard]] auto get_dr() const { return _dr; }
private:
    IndexVector _size;
    RealVector _dr;
};

struct LaserPulse {
    float intensity;
    float length;
    float cells_per_wavelength;

    [[nodiscard]] RealVector get(const float t, const RealVector& r, const RealVector& dr) const {
        const float phase = 2 * pi * t / (cells_per_wavelength / dr[0]);
        const float envelope = sqr(std::sin(pi*t/length));
        return intensity * envelope * RealVector{std::cos(phase), std::sin(phase), 0};
    }
};

void init_fields(Grid* grid){
    const IndexVector grid_size = grid->size();
    const auto ZMAX = grid_size[2]; // GCC won't vectorize with grid_size[2] as a limit

    for(int i = 0; i < grid_size[0]; ++i){
        for(int j = 0; j < grid_size[1]; ++j){
            #pragma GCC ivdep
            for(int k = 0; k < ZMAX; ++k){
                grid->E[i, j, k] = RealVector::Zeros();
                grid->B[i, j, k] = RealVector::Zeros();
                grid->J[i, j, k] = RealVector::Zeros();
            }
        }
    }
}

void clear_fields(Grid* grid){
    const IndexVector grid_size = grid->size();
    const auto Z_MAX = grid_size[2]; // GCC won't vectorize with grid_size[2] as a limit

    for(int i = 0; i < grid_size[0]; ++i){
        for(int j = 0; j < grid_size[1]; ++j){
            #pragma GCC ivdep
            for(int k = 0; k < Z_MAX; ++k){
                grid->J[i, j, k] = RealVector::Zeros();
            }
        }
    }

}

void emit(Grid* grid, LaserPulse* source, const float dt, const float t){
    const IndexVector grid_size = grid->size();
    const auto Y_MAX = grid_size[1]; // GCC won't vectorize with grid_size[1] as a limit

    // Source is emitting from the z=0 boundary
    for(int i = 0; i < grid_size[0]; ++i){
        #pragma GCC ivdep
        for(int j = 0; j < Y_MAX; ++j){
            auto r = cwise_product(grid->get_dr(),(RealVector{(float)i, (float)j, 0}));
            auto pulse = source->get(t, r, grid->get_dr());
            grid->J[i, j, 1] = 2 * pi * dt * pulse;
        }
    }
}

inline void advance_B_half(Grid *grid, const IndexVector &index, const float dt, const RealVector& idr){
    grid->B[index] += 0.5 * dt * rot<1>(grid->E, index, idr, 1);
}

inline void advance_E(Grid *grid, const IndexVector &index, const float dt, const RealVector& idr){
    grid->E[index] += 2*pi*dt*grid->J[index] + dt * rot<1>(grid->B, index, idr, -1);
}

void advance_fields(Grid *grid, const float dt){
    const RealVector idr = cwise_divide(RealVector::Ones(), grid->get_dr());
    const IndexVector grid_size = grid->size();
    const auto Z_MAX = grid_size[2]; // GCC won't vectorize with grid_size[2] as a limit

    for(int i=0; i < grid_size[0] - 1; ++i){
        for(int j=0; j < grid_size[1] - 1; ++j){
            #pragma GCC ivdep
            for(int k=0; k < Z_MAX - 1; ++k){
                advance_B_half(grid, IndexVector(i, j, k), dt, idr);
            }
        }
    }
    for(int i=1; i < grid_size[0]; ++i){
        for(int j=1; j < grid_size[1]; ++j){
            #pragma GCC ivdep
            for(int k=1; k < Z_MAX; ++k){
                advance_E(grid, IndexVector(i, j, k), dt, idr);
            }
        }
    }
    for(int i=0; i < grid_size[0] - 1; ++i){
        for(int j=0; j < grid_size[1] - 1; ++j){
            #pragma GCC ivdep
            for(int k=0; k < Z_MAX - 1; ++k){
                advance_B_half(grid, IndexVector(i, j, k), dt, idr);
            }
        }
    }
}

#ifdef HAS_HDF5
void output(const Grid* grid, const int step){
    std::print(">");
    auto base_name = std::format("simulation_results_{}", step);
    auto file = create_output_file(base_name);

    // Write the vector fields
    serialize_to_hdf5(grid->E, file, "Electric Field", IndexVector::Ones(), IndexVector(grid->E.size()) - IndexVector::Ones());
    serialize_to_hdf5(grid->B, file, "Magnetic Field", IndexVector::Ones(), IndexVector(grid->B.size()) - IndexVector::Ones());
    serialize_to_hdf5(grid->J, file, "Current Density", IndexVector::Ones(), IndexVector(grid->J.size()) - IndexVector::Ones());

    auto xdmf = XdmfWriter<IndexVector, RealVector>(base_name + ".xdmf", base_name + ".h5");
    xdmf.write(grid->size(), grid->get_dr());

    std::print("|");
}
#else
void output(const Grid* grid, const int step){
    std::println("HDF5 support not enabled, skipping output");
}
#endif

int main(int argc, char* argv[]){

    std::print("Ultimate Container FDTD simulation example\n");

    CmdLineParser parser(argc, argv);

    int num_steps = parser.get("num_steps", 100);
    RealVector num_cells = parser.get("num_cells", RealVector{80, 80, 80});
    RealVector delta_r = parser.get("delta_r", RealVector{0.1, 0.1, 0.1});
    float pulse_intensity = parser.get("pulse_intensity", 100.0f);
    float pulse_length = parser.get("pulse_length", 3.0f);
    float cells_per_wavelength = parser.get("cells_per_wavelength", 15.0f);


    std::println("Initializing simulation");

    auto grid = std::make_unique<Grid>(num_cells, delta_r);

    init_fields(grid.get());

    std::println("Running simulation, num_steps = {}", num_steps);

    auto source = std::make_unique<LaserPulse>(LaserPulse{pulse_intensity, pulse_length, cells_per_wavelength});
    auto dr = grid->get_dr();
    float dt = dt_factor / std::sqrt(1/sqr(dr[0]) + 1/sqr(dr[1]) + 1/sqr(dr[2]));

    for(int n = 0; n < num_steps; ++n){

        auto t = dt * static_cast<float>(n);
        clear_fields(grid.get());
        emit(grid.get(), source.get(), dt, t);
        advance_fields(grid.get(), dt);

        if (n % 10 == 0){
            std::print(" {} ", n);
            output(grid.get(), n);
        } else {
            std::print(".");
        }
        std::flush(std::cout);
    }

    std::println("{}", num_steps);
    std::println("Simulation finished");

    output(grid.get(), num_steps);

    return 0;
}
