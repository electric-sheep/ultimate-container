#pragma once

#include "ultimate_container/vector/vector.h"
#include "ultimate_container/utils/concepts.h"
#include <sstream>
#include <string>

// Function to parse "x, y, z" format string into a Vector<T, 3>
template<typename T>
requires ultimate_container::Arithmetic<typename T::value_type>
T parse_vector(const std::string& input) {
    using Scalar = typename T::value_type;
    Scalar x, y, z;
    char comma1, comma2;
    std::istringstream iss(input);
    if (iss >> x >> comma1 >> y >> comma2 >> z) {
        return ultimate_container::vector::Vector<Scalar, 3>{x, y, z};
    }
    throw std::runtime_error("Failed to parse vector from string");
}
