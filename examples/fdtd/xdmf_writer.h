#pragma once

#include "ultimate_container/vector/vector.h"
#include "ultimate_container/utils/concepts.h"

#include <string_view>
#include <format>
#include <fstream>

template<ultimate_container::IsVector VIDX, ultimate_container::IsVector VPOS>
class XdmfWriter {

private:
    std::ofstream _outfile;
    std::string _hdf5_filename;

    std::string vector_field(std::string_view field_name) {
        return std::format(_xdmf_template_fields_vector,
                           _hdf5_filename,
                           field_name,
                           field_name);
    }

public:
    XdmfWriter(const std::string& filename, const std::string& hdf5_filename)
        : _hdf5_filename(hdf5_filename)
    {
        _outfile.open(filename);
    }


    void write(const VIDX& full_size, const VPOS& dr) {

        using namespace ultimate_container::vector::operations;

        VIDX size = full_size - 2 * VIDX::Ones();
        VIDX origin = VIDX::Ones();

        std::string attributes;
        attributes += vector_field("Electric Field");
        attributes += vector_field("Magnetic Field");
        attributes += vector_field("Current Density");

        std::string fields = std::format(_xdmf_templates_fields_main,
                                         _hdf5_filename,
                                         size[0],
                                         size[1],
                                         size[2],
                                         dr[0],
                                         dr[1],
                                         dr[2],
                                         origin[0],
                                         origin[1],
                                         origin[2],
                                         attributes);
        _outfile << fields;

    }

    /** Field XDMF file skeleton. The placeholders are:
      *  {0} - HDF5 file name
      *  {1} {2} {3} - domain size
      *  {4} {5} {6} - cell size
      *  {7} - field attributes
      */
    static constexpr std::string_view _xdmf_templates_fields_main =
            "<?xml version=\"1.0\" ?>\n"
            "<!DOCTYPE Xdmf SYSTEM \"Xdmf.dtd\" [\n"
            "\t<!ENTITY cZYX \"{3} {2} {1}\">\n"    // Domain size (number of cells)
            "\t<!ENTITY cDZDYDX \"{6} {5} {4}\">\n" // Cell size
            "\t<!ENTITY pOXYZ \"{7} {8} {9}\">\n"   // Domain origin (real-world)
            "]>\n"
            "<Xdmf xmlns:xi=\"http://www.w3.org/2001/XInclude\" Version=\"2.2\">\n"
            "<!-- HDF5 file name: {0} -->\n"
            "<Domain>\n"
            "\t<Grid Name=\"Grid\">\n"
            "\t\t<Topology TopologyType=\"3DCoRectMesh\" Dimensions=\"&cZYX;\"/>\n"
            "\t\t<Geometry GeometryType=\"ORIGIN_DXDYDZ\">\n"
            "\t\t\t<DataItem Dimensions=\"3\" NumberType=\"Float\" Precision=\"8\" Format=\"XML\">\n"
            "\t\t\t\t&pOXYZ;\n"
            "\t\t\t</DataItem>\n"
            "\t\t\t<DataItem Dimensions=\"3\" NumberType=\"Float\" Precision=\"8\" Format=\"XML\">\n"
            "\t\t\t\t&cDZDYDX;\n"
            "\t\t\t</DataItem>\n"
            "\t\t</Geometry>\n"
            "{10}"   // Field attributes
            "\t</Grid>\n"
            "</Domain>\n"
            "</Xdmf>\n";

    /** Vector attribute skeleton for fields. The placeholders are:
     *  {0} - HDF5 file name
     *  {1} - attribute name
     *  {2} - dataset name
     */
    static constexpr std::string_view _xdmf_template_fields_vector =
            "\t\t<Attribute Name=\"{1}\" AttributeType=\"Vector\" Center=\"Cell\">\n" // Attribute name
            "\t\t\t<DataItem ItemType=\"Function\" Function=\"JOIN($2, $1, $0)\" Dimensions=\"&cZYX; 3\">\n"
            "\t\t\t\t<DataItem Dimensions=\"&cZYX;\" NumberType=\"Float\" Precision=\"8\" Format=\"HDF\">\n"
            "\t\t\t\t\t{0}:/{2}/0\n" // HDF5 file name, field group name
            "\t\t\t\t</DataItem>\n"
            "\t\t\t\t<DataItem Dimensions=\"&cZYX;\" NumberType=\"Float\" Precision=\"8\" Format=\"HDF\">\n"
            "\t\t\t\t\t{0}:/{2}/1\n" // HDF5 file name, field group name
            "\t\t\t\t</DataItem>\n"
            "\t\t\t\t<DataItem Dimensions=\"&cZYX;\" NumberType=\"Float\" Precision=\"8\" Format=\"HDF\">\n"
            "\t\t\t\t\t{0}:/{2}/2\n" // HDF5 file name, field group name
            "\t\t\t\t</DataItem>\n"
            "\t\t\t</DataItem>\n"
            "\t\t</Attribute>\n";

};