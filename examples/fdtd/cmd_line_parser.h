#pragma once

#include "parse_vector.h"
#include "ultimate_container/utils/concepts.h"

#include <iostream>
#include <sstream>
#include <string>
#include <unordered_map>
#include <charconv>
#include <type_traits>
#include <stdexcept> // for std::runtime_error

class CmdLineParser {
private:
    std::unordered_map<std::string, std::string> args;

public:
    CmdLineParser(int argc, char* argv[]) {
        for (int i = 1; i < argc; ++i) {
            std::string arg = argv[i];
            size_t pos = arg.find('=');
            if (pos != std::string::npos) {
                std::string key = arg.substr(0, pos);
                std::string value = arg.substr(pos + 1);
                args[key] = value;
            }
        }
    }

    // Revised parse method that throws an exception on failure
    template<typename T>
    requires std::is_arithmetic_v<T> || ultimate_container::IsVector<T>
    T parse(const std::string& key) const {
        auto it = args.find(key);
        if (it == args.end()) {
            throw std::runtime_error("Key not found: " + key);
        }

        T value;
        if constexpr (std::is_arithmetic_v<T>) {
            std::istringstream ss(it->second);
            if (!(ss >> value)) {
                throw std::runtime_error("Failed to parse arithmetic value for key: " + key);
            }
        } else if constexpr (ultimate_container::IsVector<T>) {
            value = parse_vector<T>(std::string(it->second));
        }
        return value;
    }

    // Get method with default value, using the new parse method
    template<typename T>
    T get(const std::string& key, const T& default_value) const {
        try {
            return parse<T>(key);
        } catch (const std::exception& e) {
            std::cerr << e.what() << ". Using default value: " << default_value << "." << std::endl;
            return default_value;
        }
    }
};
