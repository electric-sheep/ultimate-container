//
// Created by strazce on 04/07/23.
//

#pragma once

#include <algorithm>
#include <cassert>
#include <concepts>
#include <iostream>
#include <numeric>
#include <ranges>
#include <span>
#include <tuple>
#include <type_traits>
#include <utility>
#include <vector>
#include <experimental/type_traits>

#include "concepts.h"
#include "../utils/metaprog.h"
#include "../utils/concepts.h"
#include "../vector/vector.h"

namespace ultimate_container::container_soa {

    using namespace ultimate_container::utils;

    //TODO: Check if the allocation is properly aligned

    /** ContainerSoa is a class that stores the data in SoA format.
     * @tparam T Type of the data stored in the container.
     * @tparam DIM Dimension of the container.
     */
    template<typename T, std::size_t DIM> class ContainerSoa {};

    /** ContainerSoaTraits is a traits class that extracts the types from the container.
     *
     * A general type to be stored in ContainerSoa has to implement the SoaReady concept.
     *
     * @tparam T Type of the data stored in the container.
     * @tparam DIM Dimension of the container.
     */
    template<typename T, std::size_t DIM>
    struct ContainerSoaTraits {
        using value_type = typename T::value_type;
        using reference = typename T::reference;
        using const_reference = typename T::const_reference;
        using soa_tuple = typename T::soa_tuple;
    };

    /** Specialization of ContainerSoa for SoA-ready arithmetic types.
     *
     * A vector has an auto-generated soa_tuple deduced from its dimension and inner value type
     *
     * @tparam T Type of the data stored in the container.
     * @tparam DIM Dimension of the container.
     *
     * @todo Does this mean that SoaReady is a property of a Vector that allows the usage of gen_tuple?
     */
    template<SoaReady T, std::size_t DIM>
    struct ContainerSoaTraits<T, DIM> {
        using value_type = typename T::value_vector;
        using reference = typename T::reference_vector;
        using const_reference = typename T::const_reference_vector;
        using inner_container = ContainerSoa<typename T::value_type, DIM>;
        using soa_tuple = utils::gen_tuple<T::dimension, inner_container>::type;
    };


    /** Specialization of ContainerSoa for arithmetic types.
     *
     * @tparam T Type of the data stored in the container.
     * @tparam DIM Dimension of the container.
     */
    template <Arithmetic T, std::size_t DIM>
    requires (!std::is_const_v<T> && !std::is_reference_v<T>)
    class ContainerSoa<T, DIM> {
    public:
        using value_type = T;
        using reference = T&;
        using const_reference = const T&;
        static const int dimension = DIM;

        using Flat = std::vector<value_type>;

        /// @todo Document why exactly do we need the no-parameter constructor
        ContainerSoa() : _flat(DIM) {}

        /**  Constructs a multidimensional array, with dimension lengths specified as separate arguments
         *
         *   This method initializes the underlying ContainerSoa::Flat storage of a size computed by
         *   multiplying all constructor arguments. the ContainerSoa::Multi DIM-dimensional view is then
         *   initialized from the flat storage
         *
         *   @tparam Args variadic argument pack
         *   @param  n    variadic - lengths in successive dimensions
         */
        template <typename... Args>
        requires (std::convertible_to<Args, std::size_t> && ...)
        ContainerSoa(Args... n) {
            resize(n...);
        }

        template <typename V, size_t... Is>
        void resize_by_vector(V v, std::index_sequence<Is...> ) {
            resize(static_cast<std::size_t>(v[Is])...);   // Will call resize(v[0],v[1],v[2],...). For v=(5,6,7): resize(5,6,7)
        }

        template<typename VT>
        explicit ContainerSoa(const vector::Vector<VT, DIM>& v) {
            resize_by_vector(v, std::make_index_sequence<DIM>{} );
        }

        auto&& linear_index(this auto&& self, std::size_t i) {
            assert(i < self._flat.size());
            return std::forward<decltype(self)>(self)._flat[i];
        }

        auto&& operator[](this auto&& self, std::size_t i)
        requires (DIM == 1) {
            return std::forward<decltype(self)>(self).linear_index(i);
        }

        template <typename VT>
        auto&& operator[](this auto&& self, const vector::Vector<VT, DIM>& index) {
            std::size_t linear_index = index[0];
            assert(linear_index < self._flat.size());
            unroll ([&] <size_t I> () {
                    linear_index += index[I+1] * self._strides[I+1];
                }, std::make_index_sequence <DIM-1> {});
            assert(linear_index < self._flat.size());
            return std::forward<decltype(self)>(self).linear_index(linear_index);
        }

        template <std::integral... Ints>
        auto&& operator[](this auto&& self, Ints... indices)
        requires (sizeof...(Ints) > 1 && sizeof...(Ints) == DIM) {  // Only allow this for multidimensional containers
            static_assert(sizeof...(Ints) == DIM, "Incorrect number of indices provided");
            return std::forward<decltype(self)>(self)[vector::Vector<std::size_t, DIM>{(static_cast<std::size_t>(indices))...}];
        }

        auto size() const {
            return _size;
        }

        bool empty() const {
            return _flat.empty();
        }

        void pop_back() {
            static_assert(DIM == 1, "Cannot pop_back from multidimensional container!");
            _flat.pop_back();
            --_size[0];
        }

        template <typename... Args>
        requires (std::convertible_to<Args, std::size_t> && ...)
        void resize(Args... n) {
            _size = {static_cast<std::size_t>(n)...};
            std::exclusive_scan(_size.begin(), _size.end(), _strides.begin(), 1, std::multiplies<>());
            const std::size_t flat_size = (n * ...);
            _flat.resize(flat_size);
        }

        template<typename Tuple>
        void resize_from_tuple(Tuple&& t) {
            std::apply([this](auto... args) { resize(args...); }, std::forward<Tuple>(t));
        }

        // Apply a given function to the arithmetic data with nesting information
        template <typename Func>
        void apply_to_components_with_nesting(Func&& f, std::string_view parent_name = "") const {
            f(_flat, parent_name);  // Arithmetic types don't have nested components
        }

        /** Get a pointer to the raw data.
         *
         * This is a dangerous function that only makes sense when the Flat type provides a pointer to the raw data.
         * It is useful for interfacing with external libraries that require raw pointers - specifically FFTW.
         * Although it could have been used for HDF5 serialization, we have a better abstraction for that - the
         * apply_to_components_with_nesting method, where the serialization function is applied to the _flat
         * component. A similar approach might work for FFTW as well if we can encapsulate all FFTW calls
         * into a single block. Right now the pointer has to persist between calls to FFTW functions, so there's
         * no other solution than to return it directly.
         *
         * @return Pointer to the raw data.
         */
        value_type* get_raw_data_pointer() {
            return _flat.data();
        }

        std::array<value_type*, 1> get_raw_data_pointers() {
            return { get_raw_data_pointer() };
        }

    private:
        Flat _flat;
        std::array<std::size_t, DIM> _size;
        std::array<std::size_t, DIM> _strides;
    };


    /** Multi-dimensional container for compound types.
     *
     * @tparam T Type of the data stored in the container.
     * @tparam DIM Dimension of the container.
     */
    template<NonArithmetic T, std::size_t DIM>
    class ContainerSoa<T, DIM> {
    public:
        using value_type = typename ContainerSoaTraits<T,DIM>::value_type;
        using reference = typename ContainerSoaTraits<T,DIM>::reference;
        using const_reference = typename ContainerSoaTraits<T,DIM>::const_reference;
        static const int dimension = DIM;

        using Data = typename ContainerSoaTraits<T,DIM>::soa_tuple;

        /** Resize the container.
         *
         * @param n0 Size of the first dimension.
         * @param n  Sizes of the remaining dimensions.
         */
        template<typename ... N>
        void resize(N ... n) {
            static_assert(sizeof...(N) == DIM, "Wrong number of dimensions");
            static_assert((std::is_same_v < std::size_t, N > && ...), "All dimensions must be of type std::size_t");
            _n = (n * ...);    // Multiply all dimensions
            _size = {n...};    // Store the sizes
            // Call the .resize() method of each element of the _data tuple
            std::apply([&](auto&... args) { (args.resize(n...), ...); }, _data);
        }

        ContainerSoa() = default;

        /** Default constructor.
         *
         * @param n0 Size of the first dimension.
         * @param n  Sizes of the remaining dimensions.
         */
        template<typename ... N>
        explicit ContainerSoa(std::size_t n0, N ... n) {
            static_assert(sizeof...(N) == DIM - 1, "Wrong number of dimensions");
            static_assert((std::is_same_v < std::size_t, N > && ...), "All dimensions must be of type std::size_t");
            resize(n0, n...);
        }


////////////// The following is unusable because I can't get Vector::get<I> to work
//        /** Constructor from a Vector.
//         *
//         * @tparam VT Type of the Vector.
//         * @tparam VSOA Storage order of the Vector.
//         * @param v Vector containing the sizes of the dimensions.
//         */
//        template <typename VT, bool VSOA>
//        ContainerSoa(const vector::Vector<VT, DIM, VSOA>& vec)
//        requires (std::is_convertible_v<VT, std::size_t>)
//        {
//            std::apply([this](auto... args) { resize(args...); }, vec);
//        }


        template <typename V, size_t... Is>
        void resize_by_vector(V v, std::index_sequence<Is...> ) {
            resize(static_cast<std::size_t>(v[Is])...);   // Will call resize(v[0],v[1],v[2],...). For v=(5,6,7): resize(5,6,7)
        }

        template<typename VT>
        explicit ContainerSoa(const vector::Vector<VT, DIM>& v){
            resize_by_vector(v, std::make_index_sequence<DIM>{} );
        }
////////////// The previous two functions are temporary until I can get Vector::get<I> to work.

        auto size() const {
            return _size;
        }

        bool empty() const {
            return _n == 0;
        }

        template <std::size_t... I>
        inline const_reference tuple_to_vector(std::size_t i, std::index_sequence<I...>) const {
            assert(i < _n);
            return const_reference(std::get<I>(_data)[i]...);
        }

        template <std::size_t... I>
        inline reference tuple_to_vector(std::size_t i, std::index_sequence<I...>) {
            assert(i < _n);
            return reference(std::get<I>(_data)[i]...);
        }


        reference operator[](std::size_t i) {
            assert(i < _n);
            return tuple_to_vector(i, std::make_index_sequence<std::tuple_size_v<Data>>());
        }

        const_reference operator[](std::size_t i) const {
            assert(i < _n);
            return tuple_to_vector(i, std::make_index_sequence<std::tuple_size_v<Data>>());
        }


        // Indexing by vector
        template<std::size_t ... I, typename IDX>
        reference tuple_to_vector(IDX idx, std::index_sequence<I...>) {
            return reference(std::get<I>(_data)[idx] ...);
        }

        template<std::size_t ... I, typename IDX>
        const_reference tuple_to_vector(IDX idx, std::index_sequence<I...>) const {
            return const_reference(std::get<I>(_data)[idx] ...);
        }

        template<typename VT>
        reference operator[](const vector::Vector<VT, DIM>& vec) {
            return tuple_to_vector(vec, std::make_index_sequence<std::tuple_size_v<Data>>());
        }

        template<typename VT>
        const_reference operator[] (const vector::Vector<VT, DIM>& vec) const {
            return tuple_to_vector(vec, std::make_index_sequence<std::tuple_size_v<Data>>());
        }

        template<typename... Ints> requires (std::integral<Ints> && ...)
        reference operator[](Ints... indices) {
            static_assert(sizeof...(Ints) == DIM, "Number of indices must match the container dimension");
            return (*this)[vector::Vector<std::size_t, DIM>{(static_cast<std::size_t>(indices))...}];
        }

        template<typename... Ints> requires (std::integral<Ints> && ...)
        const_reference operator[](Ints... indices) const {
            static_assert(sizeof...(Ints) == DIM, "Number of indices must match the container dimension");
            return (*this)[vector::Vector<std::size_t, DIM>{(static_cast<std::size_t>(indices))...}];
        }

        // Public interface to start the apply operation
        template <typename Func>
        void apply_to_components_with_nesting(Func&& f, std::string_view parent_name = "") const {
            apply_to_components_with_nesting_impl(
                    std::forward<Func>(f),
                    parent_name,
                    std::make_index_sequence<std::tuple_size_v<Data>>{});
        }

        auto get_raw_data_pointers() {
            return get_raw_data_pointers_impl(std::make_index_sequence<std::tuple_size_v<Data>>{});
        }

        template<std::size_t I>
        auto* get_component() {
            return &(std::get<I>(_data));
        }

    private:
        Data _data;
        std::array<std::size_t, DIM> _size;
        std::size_t _n = 0; // Total number of elements, must be kept in sync with _size.

        /* This solution uses a helper struct and std::optional to handle cases where a struct may or may not have
         * defined member names. It's a workaround due to the lack of static reflection in current C++ standards.
         * Once C++ supports static reflection, this workaround can be removed. */
        // Define the trait to check
        template<typename U> using has_member_names_t = decltype(U::member_names);
        // Use the detector
        static constexpr bool has_member_names_v = std::experimental::is_detected_v<has_member_names_t, T>;
        // Helper struct to delay the evaluation of decltype(T::member_names)
        template<bool B, typename U> struct member_names_helper {};
        template<typename U> struct member_names_helper<true,  U> { using type = decltype(U::member_names); };
        template<typename U> struct member_names_helper<false, U> { using type = std::false_type; };
        using member_names_t = typename member_names_helper<has_member_names_v, T>::type;
        static constexpr auto get_member_names() {
            if constexpr (has_member_names_v) {
                return std::optional<member_names_t>(T::member_names);
            } else {
                return std::optional<member_names_t>{std::nullopt};
            }
        }
        static constexpr auto member_names = get_member_names();

        // Apply a given function to the compound type's components with nesting information
        template <typename Func, std::size_t... Is>
        void apply_to_components_with_nesting_impl(Func&& f, std::string_view parent_name, std::index_sequence<Is...>) const {
            // Apply the function `f` for each element in the tuple, appending the index to the parent_nesting string
            (..., apply_to_component<Is>(std::forward<Func>(f), parent_name));
        }

        template <std::size_t I, typename Func>
        void apply_to_component(Func&& f, std::string_view parent_name) const {
            if constexpr (has_member_names_v) {
                f(std::get<I>(_data), std::string(parent_name) + (parent_name.empty() ? "" : "/") + std::get<I>(*member_names));
            } else {
                f(std::get<I>(_data), std::string(parent_name) + (parent_name.empty() ? "" : "/") + std::to_string(I));
            }
        }

        template <std::size_t... Is>
        auto get_raw_data_pointers_impl(std::index_sequence<Is...>) {
            return std::array{ std::get<Is>(_data).get_raw_data_pointer()... };
        }
    };
}

