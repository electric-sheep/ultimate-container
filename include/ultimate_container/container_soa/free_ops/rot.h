#pragma once

#include "ultimate_container/vector/vector.h"

namespace ultimate_container::container_soa {

    namespace details {
        template<
                std::size_t Order,
                typename CONTAINER,
                std::size_t CDIM,
                typename IDX,
                typename DIFF>
        struct RotImpl {
        };

        template<typename CONTAINER, typename IDX, typename DIFF>
        struct RotImpl<1, CONTAINER, 3, IDX, DIFF> {
            using VEC = typename CONTAINER::value_type;

            inline VEC operator()(const CONTAINER &f, const IDX &index, const DIFF &inverse_dr, const int direction) {
                static_assert(VEC::dimension == 3, "Vector rotation undefined for vectors of DIM != 3.");

                const int i = index[0];
                const int j = index[1];
                const int k = index[2];

                const int ii = index[0] + direction;
                const int jj = index[1] + direction;
                const int kk = index[2] + direction;

                return VEC(
                          (f[index][2] - f[IDX(i, jj, k)][2]) * inverse_dr[1]
                        - (f[index][1] - f[IDX(i, j, kk)][1]) * inverse_dr[2],

                          (f[index][0] - f[IDX(i, j, kk)][0]) * inverse_dr[2]
                        - (f[index][2] - f[IDX(ii, j, k)][2]) * inverse_dr[0],

                          (f[index][1] - f[IDX(ii, j, k)][1]) * inverse_dr[0]
                        - (f[index][0] - f[IDX(i, jj, k)][0]) * inverse_dr[1]
                );
            }
        };

        template<typename CONTAINER, typename IDX, typename DIFF>
        struct RotImpl<1, CONTAINER, 2, IDX, DIFF> {
            using VEC = typename CONTAINER::value_type;

            inline VEC operator()(const CONTAINER &f, const IDX &index, const DIFF &inverse_dr, const int direction) {
                static_assert(VEC::dimension == 3, "Vector rotation undefined for vectors of DIM != 3.");

                const int i = index[0];
                const int j = index[1];

                const int ii = index[0] + direction;
                const int jj = index[1] + direction;

                return VEC(
                        (f[index][2] - f[IDX(i, jj)][2]) * inverse_dr[1],

                        (f[IDX(ii, j)][2] - f[index][2]) * inverse_dr[0],

                        (f[index][1] - f[IDX(ii, j)][1]) * inverse_dr[0]
                      - (f[index][0] - f[IDX(i, jj)][0]) * inverse_dr[1]

                );
            }
        };

        template<typename CONTAINER, typename IDX, typename DIFF>
        struct RotImpl<1, CONTAINER, 1, IDX, DIFF> {
            using VEC = typename CONTAINER::value_type;

            inline VEC operator()(const CONTAINER &f, const IDX &index, const DIFF &inverse_dr, const int direction) {
                static_assert(VEC::dimension == 3, "Vector rotation undefined for vectors of DIM != 3.");

                const int ii = index[0] + direction;

                return VEC(
                        0,

                        (f[IDX(ii)][2] - f[index][2]) * inverse_dr[0],

                        (f[index][1] - f[IDX(ii)][1]) * inverse_dr[0]

                );
            }
        };
    }

    template<size_t Order, typename T, size_t DIM, typename IDX, typename DIFF>
    inline auto static rot(const ContainerSoa<T, DIM> &field,
                           const IDX &index,
                           const DIFF &inverse_dr,
                           const int direction) {
        details::RotImpl<Order, ContainerSoa<T, DIM>, DIM, IDX, DIFF> rot_impl;
        return rot_impl(field, index, inverse_dr, direction);
    }

}
