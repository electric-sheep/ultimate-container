#pragma once

#include <H5Cpp.h>

namespace ultimate_container::serialization {

template<typename T>
struct NativeTypeFor {
    static_assert(sizeof(T) == 0, "Unsupported type");
    static H5::DataType type()
    {
        return H5::PredType::NATIVE_DOUBLE; // this line will never be reached, but we need it to compile
    }
};

template<> struct NativeTypeFor<float>          { static inline H5::FloatType type() { return H5::PredType::NATIVE_FLOAT; }};
template<> struct NativeTypeFor<double>         { static inline H5::FloatType type() { return H5::PredType::NATIVE_DOUBLE; }};
template<> struct NativeTypeFor<long double>    { static inline H5::FloatType type() { return H5::PredType::NATIVE_LDOUBLE; }};

template<> struct NativeTypeFor<char>               { static inline H5::IntType type() { return H5::PredType::NATIVE_CHAR; }};
template<> struct NativeTypeFor<unsigned char>      { static inline H5::IntType type() { return H5::PredType::NATIVE_UCHAR; }};
template<> struct NativeTypeFor<short>              { static inline H5::IntType type() { return H5::PredType::NATIVE_SHORT; }};
template<> struct NativeTypeFor<unsigned short>     { static inline H5::IntType type() { return H5::PredType::NATIVE_USHORT; }};
template<> struct NativeTypeFor<int>                { static inline H5::IntType type() { return H5::PredType::NATIVE_INT; }};
template<> struct NativeTypeFor<unsigned int>       { static inline H5::IntType type() { return H5::PredType::NATIVE_UINT; }};
template<> struct NativeTypeFor<long>               { static inline H5::IntType type() { return H5::PredType::NATIVE_LONG; }};
template<> struct NativeTypeFor<unsigned long>      { static inline H5::IntType type() { return H5::PredType::NATIVE_ULONG; }};
template<> struct NativeTypeFor<long long>          { static inline H5::IntType type() { return H5::PredType::NATIVE_LLONG; }};
template<> struct NativeTypeFor<unsigned long long> { static inline H5::IntType type() { return H5::PredType::NATIVE_ULLONG; }};

template<> struct NativeTypeFor<bool>           { static inline H5::IntType type() { return H5::PredType::NATIVE_INT; }};


}