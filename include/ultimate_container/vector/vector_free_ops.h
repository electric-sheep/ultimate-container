#pragma once

#include <cmath>
#include <ostream>
#include <type_traits>

#include "../utils/metaprog.h"

namespace ultimate_container::vector::operations {

using namespace ultimate_container::utils;

template<typename T1, typename T2, std::size_t DIM>
constexpr inline static auto
operator+(const Vector<T1, DIM> &a, const Vector<T2, DIM> &b) {
    using V = Vector<std::remove_cvref_t<decltype(a[0]+b[0])>, DIM>;
    V r(a);
    r += b;
    return r;
}

template<typename T1, typename T2, std::size_t DIM>
constexpr inline static auto
operator-(Vector<T1, DIM> a, Vector<T2, DIM> b) {
    using V = Vector<std::remove_cvref_t<decltype(a[0]-b[0])>, DIM>;
    V r(a);
    r -= b;
    return r;
}

template<typename T, std::size_t DIM>
constexpr inline static auto
operator-(Vector<T, DIM> a)
    -> Vector<std::remove_cvref_t<decltype(-a[0])>, DIM> {
    using V = Vector<std::remove_cvref_t<decltype(-a[0])>, DIM>;
    V r(a);
    r *= -1; ///TODO: Performance, probably better to directly initialize the vector with {-a[0]...}.
    return r;
}

template<typename T, std::size_t DIM, Arithmetic Scalar>
constexpr inline static auto
operator*(Scalar c, Vector<T, DIM> a)
    -> Vector<std::remove_cvref_t<decltype(c*a[0])>, DIM> {
    using V = Vector<std::remove_cvref_t<decltype(c*a[0])>, DIM>;
    V r(a);
    r *= c;
    return r;
}


template<typename T, std::size_t DIM, Arithmetic Scalar>
constexpr inline static auto
operator*(Vector<T, DIM> a, Scalar c) {
    return c * a;
}

template<typename T, std::size_t DIM, Arithmetic Scalar>
constexpr inline static auto
operator/(Vector<T, DIM> a, Scalar c)
    -> Vector<std::remove_cvref_t<decltype(a[0]/c)>, DIM> {
    using V = Vector<std::remove_cvref_t<decltype(a[0]/c)>, DIM>;
    V r(a);
    r /= c;
    return r;
}

template<typename T, std::size_t DIM>
requires std::is_integral_v<T>
constexpr inline static Vector<T, DIM>
operator%(const Vector<T, DIM>& vec, T scalar) {
    Vector<T, DIM> result;
    for (std::size_t i = 0; i < DIM; ++i) {
        result[i] = vec[i] % scalar;
    }
    return result;
}

template<typename T, std::size_t DIM>
requires std::is_floating_point_v<T>
constexpr inline static auto
inverse(Vector<T, DIM> a) {
    return a.inverse();
}

template<typename T1, typename T2, std::size_t DIM>
constexpr inline static auto
cwise_product(Vector<T1, DIM> a, Vector<T2, DIM> b)
-> Vector<std::remove_cvref_t<decltype(a[0]*b[0])>, DIM> {
    using V = Vector<std::remove_cvref_t<decltype(a[0]*b[0])>, DIM>;
    V r(a);
    return r.cwise_product(b);
}

template<typename T1, typename T2, std::size_t DIM>
constexpr inline static auto
cwise_divide(Vector<T1, DIM> a, Vector<T2, DIM> b)
-> Vector<std::remove_cvref_t<decltype(a[0]*b[0])>, DIM> {
    using V = Vector<std::remove_cvref_t<decltype(a[0]*b[0])>, DIM>;
    V r(a);
    return r.cwise_divide(b);
}

template<typename T1, typename T2, std::size_t DIM>
constexpr inline static auto
cwise_min(Vector<T1, DIM> a, Vector<T2, DIM> b)
-> Vector<std::remove_cvref_t<decltype(std::min(a[0],b[0]))>, DIM> {
    using V = Vector<std::remove_cvref_t<decltype(std::min(a[0],b[0]))>, DIM>;
    V r(a);
    return r.cwise_min(b);
}

template<typename T1, typename T2, std::size_t DIM>
constexpr inline static auto
cwise_max(Vector<T1, DIM> a, Vector<T2, DIM> b)
-> Vector<std::remove_cvref_t<decltype(std::min(a[0],b[0]))>, DIM> {
    using V = Vector<std::remove_cvref_t<decltype(std::max(a[0],b[0]))>, DIM>;
    V r(a);
    return r.cwise_max(b);
}

template<typename T, std::size_t DIM>
constexpr inline static auto
cwise_sqr(Vector<T, DIM> v) {
    return v.cwise_sqr();
}

//TODO: Really? Shouldn't the whole function be just a simple "return v.floor();" ???
template<typename T, std::size_t DIM>
constexpr inline static auto
floor(Vector<T, DIM> a)
-> Vector<std::remove_cvref_t<T>, DIM> {
    using V = Vector<std::remove_cvref_t<T>, DIM>;
    V r(a);
    return r.floor();
}

//TODO: Really? Shouldn't the whole function be just a simple "return v.floor();" ???
template<typename T, std::size_t DIM>
constexpr inline static auto
cwise_abs(Vector<T, DIM> a)
-> Vector<std::remove_cvref_t<T>, DIM> {
    using V = Vector<std::remove_cvref_t<T>, DIM>;
    V r(a);
    return r.cwise_abs();
}

template<typename T, std::size_t DIM>
constexpr inline static Arithmetic auto
squared_norm(Vector<T, DIM> a) {
    return a.squared_norm();
}

template<typename T, std::size_t DIM>
constexpr inline static Arithmetic auto
sum(Vector<T, DIM> a) { //TODO: STL has std::accumulate. We should implement support for it (and maybe remove this function).
    return a.sum();
}

template<typename T, std::size_t DIM>
constexpr inline static Arithmetic auto
product(Vector<T, DIM> a) {
    return a.product();
}

template<typename T1, typename T2, std::size_t DIM>
constexpr inline static auto
dot(Vector<T1, DIM> a, Vector<T2, DIM> b) {
    return a.dot(b);
}

template<typename T1, typename T2, std::size_t DIM>
constexpr inline static auto
cross(Vector<T1, DIM> a, Vector<T2, DIM> b)
-> Vector<std::remove_cvref_t<decltype(a[0]*b[0])>, DIM> {
    using V = Vector<std::remove_cvref_t<decltype(a[0]*b[0])>, DIM>;
    V r(a);
    return r.cross(b);
}

template<typename TNew, typename T, std::size_t DIM>
constexpr inline static auto
vector_cast(Vector<T, DIM> a)
-> Vector<std::remove_cvref_t<TNew>, DIM> {
    // This forces the output to be a value vector, probably creating an unexpected temporary.
    return a.template cast<std::remove_cvref_t<TNew>>();
}

template<typename T, std::size_t DIM>
inline static std::ostream &
operator<<(std::ostream & co,  Vector<T, DIM> const & v) {
    co << "(";
    co << v[0];
    for (std::size_t i=1; i<DIM; ++i) co << ", " << v[i];
    co << ")";
    return co;
}

}
