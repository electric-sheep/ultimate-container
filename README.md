# ultimate-container

A header-only C++ library for high performance simulations providing:

 * Container wrapper for SoA storage with AoS-like interface.
 * Multidimensional arrays of multidimensional vectors.
 * Expression template engine for no-overhead operations on vectors and containers.

[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.4745536.svg)](https://doi.org/10.5281/zenodo.4745536)
[![License: MPL 2.0](https://img.shields.io/badge/License-MPL_2.0-brightgreen.svg)](https://opensource.org/licenses/MPL-2.0)
[![codecov](https://codecov.io/gitlab/electric-sheep/ultimate-container/graph/badge.svg?token=yhpKZi4T8K)](https://codecov.io/gitlab/electric-sheep/ultimate-container)

Some of the basic ideas were explained in the 
[Template Metaprogramming for Massively Parallel Scientific Computing](https://vyskocil.com/icsc-2016/) lecture
presented at the *inverted CERN School of Computing 2016*. The idea for AoS-SoA binding implementation 
through value/reference objects was taken from 
Vincenzo Innocente's [UltimateSoA](https://github.com/VinInn/MultiBodyExcercise) 
as was the inspiration for the library name.

Zenodo DOI: https://doi.org/10.5281/zenodo.4745535

© 2016-2023 Jiří Vyskočil, licensed under MPL-2.0.
